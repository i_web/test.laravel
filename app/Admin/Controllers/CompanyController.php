<?php

namespace App\Admin\Controllers;

use App\Models\Company;
use App\Models\CompanyUsers;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use GuzzleHttp\Psr7\Request;

class CompanyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Company';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Company());
        $grid->column('id');
        $grid->column('name');
        $grid->column('email');
        $grid->column('created_at');
        $grid->column('updated_at');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Company::findOrFail($id));
        $show->panel()->title('Информация о компании');
        $show->field('name');
        $show->field('address');
        $show->field('email');
        $show->field('logo')->image();
        $show->users('Сотрудники компании',function (Grid $users){
            $users->disableActions();
            $users->disableBatchActions();
            $users->disableCreateButton();
            $users->disableFilter();
            $users->disableExport();
            $users->disableColumnSelector();
            $users->disablePagination();
            $users->disablePerPageSelector();
            $users->column('name');
            $users->column('email');
        });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Company());
        $form->text('name')->rules('required|min:10');
        $form->text('address');
        $form->email('email')->rules('required');
        $form->file('logo')->rules('mimes:jpeg,bmp,png,gif,svg,webp|dimensions:min_width=100,min_height=100');
        return $form;
    }
}
