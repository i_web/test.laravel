<?php

namespace App\Admin\Controllers;

use App\Models\Company;
use App\Models\CompanyUsers;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CompanyUsersController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'CompanyUsers';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CompanyUsers());

        $grid->column('id', __('Id'));
        $grid->column('id_company', __('Company'))->display(function ($company_id){
            return Company::find($company_id)->name;
        });
        $grid->column('name', __('Name'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Phone'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CompanyUsers::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('id_company', __('Company'))->as(function ($company_id){
            return Company::find($company_id)->name;
        });
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Phone'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CompanyUsers());
        $form->select('id_company',__('Company'))->options(Company::pluck('name','id'))->rules('required');
        $form->text('name', __('Name'))->rules('required');
        $form->email('email', __('Email'))->rules('required|email');
        $form->mobile('phone', __('Phone'))->rules('required');

        return $form;
    }
}
