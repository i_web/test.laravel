<?php

use Illuminate\Routing\Router;
#use App\Admin\Controllers\CompanyController;
#use App\Admin\Controllers\CompanyUsersController;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    //$router->get('/companies', '');
    $router->resource('companies', CompanyController::class);
    $router->resource('company-users', CompanyUsersController::class);

});
